def first_task():
    a = 5
    b = 7
    c = 13
    d = 3
    print("a) a * b / (c - d) = ", a * b / (c - d))
    print("б) (b + c) % (a + d) = ", (b + c) % (a + d))
    print("в) c / (c - b) = ", c / (c - b))


def second_task():
    a = 17
    c = 4
    print("a + c = ", a + c)
    print("a - c = ", a - c)
    print("a * c = ", a * c)
    print("a / c = ", a / c)
    print("a // c = ", a // c)
    print("a %/ c = ", a % c)
    print("a ** c = ", a**c)


def third_task():
    x = 29
    y = 12
    x += y
    print("x += y = ", x)
    x -= y
    print("x -= y = ", x)
    x *= y
    print("x *= y = ", x)
    x /= y
    print("x /= y = ", x)
    x %= y
    print("x %= y = ", x)
